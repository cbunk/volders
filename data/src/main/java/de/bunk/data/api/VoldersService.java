package de.bunk.data.api;

import de.bunk.data.entity.volders.SubjectResponseData;
import retrofit2.http.GET;
import rx.Observable;

public interface VoldersService {
    @GET("subjects")
    Observable<SubjectResponseData> getSubjects();
}
