package de.bunk.data.entity.volders;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@AutoValue
public abstract class SubjectResponseData {

    @SerializedName("subjects")
    public abstract List<SubjectData> dataList();

    public static TypeAdapter<SubjectResponseData> typeAdapter(Gson gson) {
        return new AutoValue_SubjectResponseData.GsonTypeAdapter(gson);
    }
}
