package de.bunk.data.entity.volders;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.Nullable;


@AutoValue
public abstract class SubjectData {

    public abstract String name();

    @SerializedName("icon_name")
    public abstract String iconName();

    @Nullable
    @SerializedName("icon_color")
    public abstract String iconColor();

    public static TypeAdapter<SubjectData> typeAdapter(Gson gson) {
        return new AutoValue_SubjectData.GsonTypeAdapter(gson);
    }
}
