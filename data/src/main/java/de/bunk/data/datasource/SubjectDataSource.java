package de.bunk.data.datasource;

import de.bunk.data.entity.volders.SubjectResponseData;
import rx.Observable;

public interface SubjectDataSource {
    Observable<SubjectResponseData> getSubjects();
}
