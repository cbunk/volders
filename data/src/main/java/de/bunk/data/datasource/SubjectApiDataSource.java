package de.bunk.data.datasource;

import de.bunk.data.api.VoldersService;
import de.bunk.data.entity.volders.SubjectResponseData;
import rx.Observable;

import static com.fernandocejas.arrow.checks.Preconditions.checkNotNull;

public class SubjectApiDataSource implements SubjectDataSource {

    private VoldersService voldersService;

    public SubjectApiDataSource(VoldersService voldersService) {
        this.voldersService = checkNotNull(voldersService);
    }

    @Override
    public Observable<SubjectResponseData> getSubjects() {
        return voldersService.getSubjects();
    }
}
