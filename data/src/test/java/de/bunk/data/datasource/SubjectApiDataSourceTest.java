package de.bunk.data.datasource;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import de.bunk.data.api.VoldersService;

public class SubjectApiDataSourceTest {

    private SubjectApiDataSource subjectApiDataSource;

    @Mock
    private VoldersService voldersService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        subjectApiDataSource = new SubjectApiDataSource(voldersService);
    }

    @Test(expected = NullPointerException.class)
    public void constructor_Should_ThrowException_When_NullIsPassed() {
        new SubjectApiDataSource(null);
    }

    @Test
    public void constructor_Should_BeSuccessful() {
        new SubjectApiDataSource(voldersService);
    }

    @Test
    public void getSubjects_Should_BeSuccessul() {
        subjectApiDataSource.getSubjects();

        Mockito.verify(voldersService).getSubjects();
    }



}