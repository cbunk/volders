package de.bunk.usecase;

import java.util.List;

import javax.inject.Inject;

import de.bunk.data.datasource.SubjectDataSource;
import de.bunk.data.entity.volders.SubjectData;
import de.bunk.data.entity.volders.SubjectResponseData;

import static com.fernandocejas.arrow.checks.Preconditions.checkNotNull;

public class GetSubjects extends UseCase {

    public interface Callback extends UseCase.ErrorCallback {
        void onSubjectsLoaded(List<SubjectData> subjectDataList);
        void onSubjectError();
    }

    private SubjectDataSource subjectDataSource;

    @Inject
    public GetSubjects(SubjectDataSource subjectDataSource) {
        this.subjectDataSource = checkNotNull(subjectDataSource);
    }

    public void execute(final Callback callback) {

        subscribe(subjectDataSource.getSubjects(), new DefaultSubscriber<SubjectResponseData>(callback, getLogger()) {
            @Override
            public void onNext(SubjectResponseData subjectDataList) {
                super.onNext(subjectDataList);

                callback.onSubjectsLoaded(subjectDataList.dataList());
            }

            @Override
            public void onError(Throwable throwable) {
                super.onError(throwable);

                callback.onSubjectError();
            }
        });
    }
}
