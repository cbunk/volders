package de.bunk.ui;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

/**
 * Created by cbunk on 15.12.15.
 */
public abstract class BaseViewHolder<T> extends RecyclerView.ViewHolder {
    public BaseViewHolder(final View itemView) {
        super(itemView);

        ButterKnife.bind(this, itemView);
    }

    protected abstract void bindData(T t, int position);
}
