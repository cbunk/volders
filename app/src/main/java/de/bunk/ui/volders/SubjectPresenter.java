package de.bunk.ui.volders;

import java.util.List;

import javax.inject.Inject;

import de.bunk.data.entity.volders.SubjectData;
import de.bunk.ui.BaseView;
import de.bunk.ui.Presenter;
import de.bunk.usecase.GetSubjects;

public class SubjectPresenter extends Presenter<SubjectPresenter.View> implements GetSubjects.Callback {

    interface View extends BaseView {
        void showSubjects(List<SubjectData> subjectDataList);
    }

    private GetSubjects getSubjects;

    @Override
    public void onSubjectsLoaded(List<SubjectData> subjectDataList) {
        View view = getView();

        if (view == null) {
            return;
        }

        view.showSubjects(subjectDataList);

    }

    @Override
    public void onSubjectError() {

    }



    @Inject
    public SubjectPresenter(GetSubjects getSubjects) {
        super(getSubjects);
        this.getSubjects = getSubjects;
    }

    public void getSubjects() {
        getSubjects.execute(this);
    }
}
