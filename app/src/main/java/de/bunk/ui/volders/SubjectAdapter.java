package de.bunk.ui.volders;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import de.bunk.data.entity.volders.SubjectData;
import de.bunk.ui.BaseAdapter;

public class SubjectAdapter extends BaseAdapter<SubjectData> {
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position, SubjectData subjectData) {
        if (holder instanceof SubjectViewHolder) {
            SubjectViewHolder subjectViewHolder = (SubjectViewHolder) holder;
            subjectViewHolder.bindData(subjectData, position);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return SubjectViewHolder.create(parent);
    }
}
