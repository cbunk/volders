package de.bunk.ui.volders;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import butterknife.Bind;
import de.bunk.ApplicationComponent;
import de.bunk.R;
import de.bunk.data.entity.volders.SubjectData;
import de.bunk.ui.BaseActivity;

public class SubjectActivity extends BaseActivity<SubjectPresenter> implements SubjectPresenter.View {

    private static final int SPAN_COUNT_TWO = 2;

    @Bind(R.id.subject_activity_recyclerview)
    RecyclerView recyclerView;
    private SubjectAdapter subjectAdapter;

    @Override
    protected int layoutToInflate() {
        return R.layout.subject_activity;
    }

    @Override
    protected void doInjection(ApplicationComponent component) {
        component.inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        recyclerView.setLayoutManager(new GridLayoutManager(this, SPAN_COUNT_TWO));
        subjectAdapter = new SubjectAdapter();
        recyclerView.setAdapter(subjectAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();

        getPresenter().getSubjects();
    }

    @Override
    public void showSubjects(List<SubjectData> subjectDataList) {
        subjectAdapter.add(subjectDataList);
    }
}
