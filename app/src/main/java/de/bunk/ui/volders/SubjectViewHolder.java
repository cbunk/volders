package de.bunk.ui.volders;

import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.Bind;
import de.bunk.R;
import de.bunk.data.entity.volders.SubjectData;
import de.bunk.ui.BaseViewHolder;

public class SubjectViewHolder extends BaseViewHolder<SubjectData> {

    @Bind(R.id.subject_item_view_content_layout)
    LinearLayout linearLayout;

    @Bind(R.id.subject_item_view_subject_icon_imageview)
    ImageView iconImageView;

    @Bind(R.id.subject_item_view_subject_name_textview)
    TextView nameTextView;

    public static SubjectViewHolder create(final ViewGroup viewGroup) {
        return new SubjectViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.subject_item_view,
                viewGroup, false));
    }

    public SubjectViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    protected void bindData(SubjectData subjectData, int position) {
        nameTextView.setText(subjectData.name());

        try {
            int color = Color.parseColor("#"+subjectData.iconColor());
            linearLayout.setBackgroundColor(color);
        } catch (IllegalArgumentException ignored) {
            Log.e("SubjectViewHolder", ignored.getMessage());

        }

        iconImageView.setImageResource(R.drawable.icon1);
    }
}
